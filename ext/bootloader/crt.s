.section ".text.crt"
.globl _start
_start:
      # set stack pointer
      # write to a2 first, to guarantee an atomic write to sp
      lui     a2, %hi(_stack_up)
      addi    a2, a2, %lo(_stack_up)
      mv      sp, a2
      jal     main
      # Alloys to stop for qemu execution
      lui	  a0, %hi(.L.str)
      addi	  a0, a0, %lo(.L.str)
      call	  put_s
1:    j       1b

.section ".spe"
.L.str:
	.asciz	"KILL\n"
	.size	.L.str, 6
