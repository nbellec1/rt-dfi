# coding=utf-8
from pathlib import Path


class FileNotFoundException( Exception ) :
    def __init__( self, path: Path ) :
        super().__init__( f"File not found : {path}" )