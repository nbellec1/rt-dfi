# coding=utf-8
from typing import NamedTuple, Set

# Types
ContextID = str

# Useful classes
class ModuloInfo( NamedTuple ) :
    min: int
    max: int
    mod: int
    rem: int

    def as_set( self ) -> Set[ int ] :
        return { i for i in range( self.min, self.max + 1, 2 ^ self.mod ) }

    def __repr__( self ) :
        return "ModuloInfo(min={min}, max={max}, mod={mod}, rem={rem})".format(
            min=hex( self.min ),
            max=hex( self.max ),
            mod=self.mod,
            rem=hex( self.rem )
        )

class ContextInfo( NamedTuple ) :
    value_set: Set[ int ]
    area_read: Set[ ModuloInfo ]
    improvement: bool


class BBData( NamedTuple ) :
    first_instruction: int
    last_instruction: int

    def __repr__( self ) :
        return "BBData(first_instruction={fi}, last_instruction={li})".format( fi=hex( self.first_instruction ),
                                                                               li=hex( self.last_instruction ) )


# Useful Constants
NS = { 'a3' : 'http://www.absint.com/a3report' }
XPATH_WCET_ANALYSIS_TASK = "a3:wcet_analysis_task"
XPATH_CFG_ANALYSIS = XPATH_WCET_ANALYSIS_TASK + "/a3:cfg_value_analysis"
XPATH_ROUTINES = XPATH_CFG_ANALYSIS + "/a3:routines/a3:routine"
XPATH_WCET_PATH = XPATH_WCET_ANALYSIS_TASK + "/a3:wcet_analysis/a3:wcet_path"
XPATH_WCET_ROUTINE_FORMAT = XPATH_WCET_PATH + "/a3:wcet_routine[@routine='{routine}']"
XPATH_WCET_CONTEXT_FORMAT = XPATH_WCET_ROUTINE_FORMAT + "/a3:wcet_context[@context='{context}']"
XPATH_WCET_START_FORMAT = XPATH_WCET_CONTEXT_FORMAT + "/a3:wcet_start"
XPATH_WCET_END_FORMAT = XPATH_WCET_CONTEXT_FORMAT + "/a3:wcet_end"
XPATH_VALUE_BLOCK = XPATH_CFG_ANALYSIS + "/a3:value_results/a3:value_block"
XPATH_VALUE_INSTRUCTION = XPATH_VALUE_BLOCK + "/a3:value_instruction"
