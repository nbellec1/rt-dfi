# coding=utf-8

# A3 interface exceptions

class MultipleEntryPointException( Exception ) :
    """ Raised if there exist multiple entry point in the program """

    def __init__( self ) :
        super().__init__( "Multiple entry points detected" )


class FailedHypothesisException( Exception ) :
    """ Raised if an hypothesis on the xml failed """

    def __init__( self, hypothesis ) :
        super().__init__( "Hypothesis : \"{hypothesis}\" failed".format( hypothesis=hypothesis ) )


class MultipleRoutineWithIdException( Exception ) :
    """ Exception used when finding multiple routines with the same id """

    def __init__( self, id_routine ) :
        super( MultipleRoutineWithIdException, self ).__init__(
            "id \"{id}\" used by multiple routines".format( id=id_routine ) )
