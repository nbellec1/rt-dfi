# coding=utf-8
from pathlib import Path
from typing import List, Optional, NamedTuple, Tuple, TypeVar, Callable

import subprocess as sp
import re
from collections import namedtuple
import pipe

from config import DEFAULT_RISCV32_OBJDUMP_PATH

LOAD_ID = (int, int, int)

ObjdumpSymbol = namedtuple( "ObjdumpSymbol", ("address", "symbol") )
ObjdumpDFILabel = namedtuple( "ObjdumpDFILabel", ("address", "label", "fid", "lid") )
ObjdumpInstruction = namedtuple( "ObjdumpInstruction", ("address", "name", "operands", "comment") )


class CheckData( NamedTuple ) :
    tag: int
    address: int

    def __repr__( self ) :
        return "CheckData(tag={tag}, address={add})".format(
            tag=self.tag,
            add=hex( self.address )
        )

class DFILoad( NamedTuple ) :
    start_add: int
    end_add: int
    check_data: List[ CheckData ]
    fid: int
    load_id: int
    intra_id: int
    offset: int

    def __repr__( self ) :
        return "DFILoad(sadd={sadd}, eadd={eadd}, fid={fid}, lid={lid}, iid={iid}, tags={tags}, offset={offset})".format(
            sadd=hex( self.start_add ),
            eadd=hex( self.end_add ),
            fid=self.fid,
            lid=self.load_id,
            iid=self.intra_id,
            tags=str( self.get_tags() ),
            offset=self.offset
        )

    def get_id( self ) -> LOAD_ID:
        return self.fid, self.load_id, self.intra_id

    def get_tags( self ) -> List[int]:
        return list(self.check_data | pipe.map(lambda x : x.tag))


class DFIStore( NamedTuple ) :
    store_add: int
    store_tag: int
    fid: int
    store_id: int
    intra_id: int

    def __repr__( self ) :
        return "DFIStore(sadd={sadd}, store_tag={store_tag}, fid={fid}, sid={iid})".format(
            sadd=hex( self.store_add ),
            store_tag=self.store_tag,
            fid=self.fid,
            sid=self.store_id,
            iid=self.intra_id
        )


def execute_objdump( binary_path: str,
                     arguments: Optional[ List[ str ] ] = None ) -> str :
    if arguments is None :
        arguments = [ ]

    p = sp.run( [ str( DEFAULT_RISCV32_OBJDUMP_PATH ) ] + arguments + [ str( binary_path ) ],
                stdout=sp.PIPE,
                text=True,
                encoding='utf-8'
                )

    return p.stdout


T = TypeVar( 'T' )


def extract_objdump_regex( elf_path: Path, args: List[ str ], regex: str, map_f: Callable[[Tuple[ str, ...]], T ] ) -> List[ T ] :
    out = execute_objdump(
        binary_path=str( elf_path ),
        arguments=args
    )
    reg = re.compile( regex, re.MULTILINE )

    return list( map( map_f, reg.findall( out ) ) )


def extract_symbols( binary_path: str ) -> List[ ObjdumpSymbol ] :
    return extract_objdump_regex(
        Path( binary_path ),
        [ "-x" ],
        r"^([0-9a-f]+) [lg].* ([._a-zA-Z0-9]*)$",
        lambda l : ObjdumpSymbol( l[ 0 ], l[ 1 ] )
    )


def extract_instructions( binary_path: str ) -> List[ ObjdumpInstruction ] :
    return extract_objdump_regex(
        Path( binary_path ),
        [ "-d" ],
        r"^([0-9a-f]+):\t[0-9a-f]+\s+(\w+)\t([0-9a-zA-Z,-]*)[ \t]*(#.*)?$",
        lambda l : ObjdumpInstruction( int( l[ 0 ], base=16 ), l[ 1 ], l[ 2 ], l[ 3 ] )
    )


def extract_dfi_labels( binary_path: str ) -> List[ ObjdumpDFILabel ] :
    return extract_objdump_regex(
        Path( binary_path ),
        [ "-x" ],
        r"^([0-9a-f]+).* (\.[be][sblc]dfi_([0-9]+)_([0-9]+))$",
        lambda l : ObjdumpDFILabel( l[ 0 ], l[ 1 ], l[ 2 ], l[ 3 ] )
    )


def extract_dfi_stores( binary_path: str ) -> List[ DFIStore ] :
    objdump_extract = "{objdump} -x {bin}".format(
        objdump=DEFAULT_RISCV32_OBJDUMP_PATH,
        bin=binary_path
    )

    store_extraction_cmd = "grep \".dfi_store\""
    awk_extract = "awk '{ print \"0x\" $1 \" : \" $5 }'"

    cmd = " | ".join( [ objdump_extract, store_extraction_cmd, awk_extract ] )

    store_end_extraction_re = re.compile(
        r"^(0x[0-9a-f]*) : .dfi_store_(tag)_([0-9]*)_([0-9]*)_([0-9]*)_?([0-9]*)$", re.MULTILINE )
    p = sp.run( cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )

    store_datas = store_end_extraction_re.findall( p.stdout )
    # store_ends = list( filter( lambda d : d[ 1 ] == "end", store_datas ) )
    store_tag = list( filter( lambda d : d[ 1 ] == "tag", store_datas ) )

    return list( map(
        lambda d : DFIStore(
            store_add=int( d[ 0 ], base=16 ),
            fid=int( d[ 2 ] ),
            store_id=int( d[ 3 ] ),
            intra_id=int( d[ 5 ] ),
            store_tag=int( d[ 4 ] )
        ),
        filter(
            lambda d : d[ 0 ] != "",
            store_tag
        ) ) )


def extract_dfi_loads( binary_path: Path ) -> List[ DFILoad ] :
    objdump_extract = f"{DEFAULT_RISCV32_OBJDUMP_PATH} -x {binary_path}"

    load_extraction_cmd = "grep \".dfi_load\""
    awk_extract = "awk '{ print \"0x\" $1 \" : \" $5 }'"

    cmd = " | ".join( [ objdump_extract, load_extraction_cmd, awk_extract ] )

    objdump_inst_extract = f"{DEFAULT_RISCV32_OBJDUMP_PATH} -d {binary_path}"

    load_inst_extraction_cmd = "grep -E 'lh'"
    awk_inst_extract = "awk '{ sub(\":\", \"\", $1) ; print \"0x\" $1 \" : \" $3 \" \" $4 }'"

    cmd_inst = " | ".join( [ objdump_inst_extract, load_inst_extraction_cmd, awk_inst_extract ] )

    load_tag_extraction_re = re.compile(
        r"^(0x[0-9a-f]*) : \.dfi_load_tag_([0-9]*)_([0-9]*)_([0-9]*)$", re.MULTILINE
    )
    load_check_extraction_re = re.compile(
        r"^(0x[0-9a-f]*) : \.dfi_load_check_([0-9]*)_([0-9]*)_([0-9]*)_([0-9]*)_([0-9]*)$", re.MULTILINE
    )
    load_end_extraction_re = re.compile(
        r"^(0x[0-9a-f]*) : \.dfi_load_end_([0-9]*)_([0-9]*)_([0-9]*)$", re.MULTILINE
    )

    load_inst_extraction_re = re.compile( r"^(0x[0-9a-f]*) : lh t4,(-?[0-9]*)\(t3\)$", re.MULTILINE )

    p = sp.run( cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )
    p_inst = sp.run( cmd_inst, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )

    load_insts = list( filter( lambda d : d[ 0 ] != "", load_inst_extraction_re.findall( p_inst.stdout ) ) )
    load_ends = load_end_extraction_re.findall( p.stdout )
    load_start = load_tag_extraction_re.findall( p.stdout )
    load_checks = load_check_extraction_re.findall( p.stdout )

    dfi_loads: List[ DFILoad ] = [ ]
    check_index = 0
    inst_index = 0
    for i in range( len( load_start ) ) :
        start_add = int( load_start[ i ][ 0 ], base=16 )
        fid = int( load_start[ i ][ 1 ] )
        load_id = int( load_start[ i ][ 2 ] )
        intra_id = int( load_start[ i ][ 3 ] )

        if fid != int( load_ends[ i ][ 1 ] ) or load_id != int( load_ends[ i ][ 2 ] ) or intra_id != int(
                load_ends[ i ][ 3 ] ) :
            raise Exception( "Start & End identifiers do not match" )

        end_add = int( load_ends[ i ][ 0 ], base=16 )

        check_data_list: List[ CheckData ] = [ ]
        while check_index < len( load_checks ) and \
                fid == int( load_checks[ check_index ][ 1 ] ) and \
                load_id == int( load_checks[ check_index ][ 2 ] ) and \
                intra_id == int( load_checks[ check_index ][ 5 ] ) :
            check_add = int( load_checks[ check_index ][ 0 ], base=16 )
            start_check_tag = int( load_checks[ check_index ][ 3 ] )
            end_check_tag = int( load_checks[ check_index ][ 4 ] )

            for check_tag in range(start_check_tag, end_check_tag+1):
                check_data_list.append( CheckData( tag=check_tag, address=check_add ) )
            check_index += 1

        while inst_index < len( load_insts ) and \
                start_add != int( load_insts[ inst_index ][ 0 ], base=16 ) :
            inst_index += 1

        offset = int( load_insts[ inst_index ][ 1 ] )

        dfi_loads.append( DFILoad( start_add=start_add,
                                   end_add=end_add,
                                   check_data=check_data_list,
                                   fid=fid,
                                   load_id=load_id,
                                   intra_id=intra_id,
                                   offset=offset ) )

    return dfi_loads
