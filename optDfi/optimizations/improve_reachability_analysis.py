# coding=utf-8

from pathlib import Path
from typing import List

from optDfi.interface.a3.context_extraction import extract_dfi_value_analysis_from_xml
from optDfi.interface.objdump import extract_dfi_loads
from optDfi.optimizations.optimization_misc import LLVM_TAG_DELETE, apply_changes


def find_tags_to_eliminate( xml_file: Path, binary_file: Path ) -> List[ LLVM_TAG_DELETE ] :
    a3_value_analysis = extract_dfi_value_analysis_from_xml( xml_file )
    binary_data = extract_dfi_loads( binary_file )

    dfi_load_db = { dfiload.start_add : dfiload for dfiload in binary_data }

    corrected_a3_value_analysis = {val: a3_value_analysis[val] for val in filter(lambda x : dfi_load_db.get( int( x, base=16 ), None) is not None, a3_value_analysis)}
    changes: List[ LLVM_TAG_DELETE ] = [ ]
    for a3_val in corrected_a3_value_analysis :
        found_tags = set()
        bin_data = dfi_load_db.get( int( a3_val, base=16 ), None)
        if bin_data is None:
            continue

        binary_tags = set( map( lambda x : x.tag, bin_data.check_data ) )

        for context_data in corrected_a3_value_analysis[ a3_val ] :
            tags = context_data.tags
            if len( tags ) == 0 :
                # a3 did not manage to detect the tags
                found_tags.update( binary_tags )
                break

            found_tags.update( tags )

        if binary_tags != found_tags :
            changes.append(
                LLVM_TAG_DELETE( bin_data.get_id(), list( binary_tags.difference( found_tags ) ) )
            )

    return changes


def improve_reachability_analysis( xml_file: Path, binary_file: Path, original_ll_file: Path,
                                   optimized_ll_file: Path, log_file: Path ) :
    changes: List[ LLVM_TAG_DELETE ] = find_tags_to_eliminate( xml_file, binary_file )

    apply_changes( original_ll_file, optimized_ll_file, changes, log_file )
