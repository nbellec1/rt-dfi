# coding=utf-8
from __future__ import annotations

import abc
import pickle
from pathlib import Path
import dataclasses
import re
import os
import subprocess as sp
import pipe

from optDfi.interface.objdump import LOAD_ID
from typing import List, Dict, Optional

METADATA_ID = int
ID = int


@dataclasses.dataclass()
class LLVM_store_tag( object ) :
    metadata_id: METADATA_ID
    distinct: bool
    store_tag: int

    _store_tag_re = re.compile( '!([0-9]*) =( distinct)? !{i32 ([0-9]*), !"dfi_store_tag"}' )

    def __str__( self ) :
        distinct_str = " distinct" if self.distinct else ""
        return f'!{self.metadata_id} ={distinct_str} !{{i32 {self.store_tag}, !"dfi_store_tag"}}'

    @staticmethod
    def match( s: str ) -> Optional[ LLVM_store_tag ] :
        m = LLVM_store_tag._store_tag_re.match( s )
        if m is not None :
            store_tag_met_id = int( m.group( 1 ) )
            distinct = m.group( 2 ) is not None
            store_tag = int( m.group( 3 ) )

            return LLVM_store_tag(
                metadata_id=store_tag_met_id,
                distinct=distinct,
                store_tag=store_tag
            )

        return None


@dataclasses.dataclass()
class LLVM_load_id( object ) :
    metadata_id: METADATA_ID
    distinct: bool
    load_id: int

    _loadid_re = re.compile( '!([0-9]*) =( distinct)? !{i32 ([0-9]*), !"dfi_load_id"}' )

    def __str__( self ) :
        distinct_str = " distinct" if self.distinct else ""
        return f'!{self.metadata_id} ={distinct_str} !{{i32 {self.load_id}, !"dfi_load_id"}}'

    @staticmethod
    def match( s: str ) -> Optional[ LLVM_load_id ] :
        m = LLVM_load_id._loadid_re.match( s )
        if m is not None :
            met_id = int( m.group( 1 ) )
            distinct = m.group( 2 ) is not None
            load_id = int( m.group( 3 ) )

            return LLVM_load_id(
                metadata_id=met_id,
                distinct=distinct,
                load_id=load_id
            )

        return None


@dataclasses.dataclass()
class LLVM_load_set( object ) :
    metadata_id: METADATA_ID
    distinct: bool
    loadid_met_id: METADATA_ID
    tag_ids: List[ METADATA_ID ]

    _loadset_re = re.compile( '!([0-9]*) =( distinct)? !{!([0-9]*), !"dfi_load_set"((, ![0-9]*)*)}' )

    def __str__( self ) :
        distinct_str = " distinct" if self.distinct else ""
        tag_list = ', !'.join( self.tag_ids | pipe.map( str ) )
        return f'!{self.metadata_id} ={distinct_str} !{{!{self.loadid_met_id}, !"dfi_load_set", !{tag_list}}}'

    @staticmethod
    def match( s: str ) -> Optional[ LLVM_load_set ] :
        m = LLVM_load_set._loadset_re.match( s )
        if m is not None :
            dfi_load_met_id = int( m.group( 1 ) )
            distinct = m.group( 2 ) is not None
            dfi_load_id_met_id = int( m.group( 3 ) )
            tag_ids = list( m.group( 4 ).split( ', !' )[ 1 : ] | pipe.map( int ) )

            return LLVM_load_set(
                metadata_id=dfi_load_met_id,
                distinct=distinct,
                loadid_met_id=dfi_load_id_met_id,
                tag_ids=tag_ids
            )

        return None


@dataclasses.dataclass()
class LLVM_metadata_db( object ) :
    dfi_store_tag_met: Dict[ METADATA_ID, LLVM_store_tag ]
    dfi_load_id_met: Dict[ METADATA_ID, LLVM_load_id ]
    dfi_load_set_met: Dict[ METADATA_ID, LLVM_load_set ]  # _, dfi_load_id, List[dfi_store_tag]
    dfi_reverse_load_id: Dict[ int, METADATA_ID ]
    dfi_reverse_store_tag_id: Dict[ int, METADATA_ID ]
    dfi_reverse_load_set_id: Dict[ METADATA_ID, METADATA_ID ]


class LLVM_abc_change( abc.ABC ) :
    @abc.abstractmethod
    def generate_sed_instruction( self, db: LLVM_metadata_db ) :
        pass


@dataclasses.dataclass
class LLVM_TAG_DELETE( LLVM_abc_change ) :
    load_bin_id: LOAD_ID
    tags_to_remove: List[ int ]

    def generate_sed_instruction( self, db: LLVM_metadata_db ) :
        load_id = self.load_bin_id[ 1 ]

        load_id_met_id = db.dfi_reverse_load_id[ load_id ]
        load_set_met_id = db.dfi_reverse_load_set_id[ load_id_met_id ]
        load_set = db.dfi_load_set_met[ load_set_met_id ]

        new_metadata = LLVM_load_set(
            metadata_id=load_set.metadata_id,
            distinct=load_set.distinct,
            loadid_met_id=load_set.loadid_met_id,
            tag_ids=list( load_set.tag_ids | pipe.where(
                lambda t : db.dfi_store_tag_met[ t ].store_tag not in self.tags_to_remove ) )
        )

        return f"s/{str( load_set )}/{str( new_metadata )}/\n"


@dataclasses.dataclass
class LLVM_TAG_CHANGE( LLVM_abc_change ) :
    previous_tag: int
    new_tag: int

    def generate_sed_instruction( self, db: LLVM_metadata_db ) :
        store_tag_met_id = db.dfi_reverse_store_tag_id[ self.previous_tag ]
        store_tag_met = db.dfi_store_tag_met[ store_tag_met_id ]

        new_store_tag = LLVM_store_tag(
            metadata_id=store_tag_met.metadata_id,
            distinct=store_tag_met.distinct,
            store_tag=self.new_tag
        )

        return f"s/{str( store_tag_met )}/{str( new_store_tag )}/\n"


@dataclasses.dataclass
class LLVM_TAG_REORDER( LLVM_abc_change ) :
    load_id: LOAD_ID
    new_tag_order: List[ int ]

    def generate_sed_instruction( self, db: LLVM_metadata_db ) :
        load_id = self.load_id[ 1 ]

        load_id_met_id = db.dfi_reverse_load_id[ load_id ]
        load_set_met_id = db.dfi_reverse_load_set_id[ load_id_met_id ]
        load_set_met = db.dfi_load_set_met[ load_set_met_id ]

        new_tag_list_order = list( self.new_tag_order | pipe.map( lambda t : db.dfi_reverse_store_tag_id[ t ] ) )
        new_tag_list = new_tag_list_order + list(
            load_set_met.tag_ids | pipe.where( lambda t : t not in new_tag_list_order ) )

        assert len(new_tag_list) == len( load_set_met.tag_ids )

        new_load_set = LLVM_load_set(
            metadata_id=load_set_met.metadata_id,
            distinct=load_set_met.distinct,
            loadid_met_id=load_set_met.loadid_met_id,
            tag_ids=new_tag_list
        )

        return f"s/{str(load_set_met)}/{str(new_load_set)}/\n"


def scan_llvm_bytecode_metadata( ll_file: Path ) :
    """ Scan the llvm textual bytecode and extract all the relevant metadata from it into a db"""
    db = LLVM_metadata_db( dict(), dict(), dict(), dict(), dict(), dict() )

    with open( ll_file, 'r', encoding='utf-8' ) as file :
        for line in file :
            # Check against store tag
            m = LLVM_store_tag.match( line )
            if m is not None :
                assert m.metadata_id not in db.dfi_store_tag_met
                db.dfi_store_tag_met[ m.metadata_id ] = m
                assert m.store_tag not in db.dfi_reverse_store_tag_id
                db.dfi_reverse_store_tag_id[ m.store_tag ] = m.metadata_id
                continue

            # Check against load id
            m = LLVM_load_id.match( line )
            if m is not None :
                assert m.metadata_id not in db.dfi_load_id_met
                db.dfi_load_id_met[ m.metadata_id ] = m
                assert m.load_id not in db.dfi_reverse_load_id
                db.dfi_reverse_load_id[ m.load_id ] = m.metadata_id
                continue

            # Check against load set
            m = LLVM_load_set.match( line )
            if m is not None :
                assert m.metadata_id not in db.dfi_load_set_met
                db.dfi_load_set_met[ m.metadata_id ] = m
                assert m.loadid_met_id not in db.dfi_reverse_load_set_id
                db.dfi_reverse_load_set_id[ m.loadid_met_id ] = m.metadata_id

    return db


def apply_changes( original_ll_file: Path, optimized_ll_file: Path, changes: List[ LLVM_abc_change ], log_changes: Optional[Path] = None ) :
    """ Generate a sed script that replace the lines in the LLVM result_file by the ones after changes and apply it"""

    if log_changes is not None:
        with open( log_changes / "llvm_changes", 'wb' ) as f:
            pickle.dump( changes, f )

    db = scan_llvm_bytecode_metadata( original_ll_file )

    sed_script = ''
    for c in changes :
        sed_script += c.generate_sed_instruction( db )

    script_sed_name = log_changes / "script_sed.sed" if log_changes is not None else "./script_sed.sed"
    with open( script_sed_name, 'w' ) as file :
        file.write( sed_script )

    sp.run( f"sed -f {script_sed_name} {original_ll_file} > {optimized_ll_file}", shell=True )
