# coding=utf-8
import pickle
from itertools import permutations, combinations
from pathlib import Path
from typing import Dict, List, Tuple, Set, FrozenSet

import pipe
import pulp
from pulp import lpSum, LpVariable, LpInteger, LpAffineExpression, LpBinary, CPLEX_CMD, LpProblem

import config
from optDfi.misc.utils import print_color, Color
from optDfi.optimizations.ilp_opt.ilp_misc import ilp_gt, ilp_absdiff, prod_var_bin, bin_or
from optDfi.optimizations.ilp_opt.ilp_problem.context import Tag, LoadId
from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import EqClassMap
from optDfi.optimizations.ilp_opt.ilp_problem.problem import IlpIntervalProblem, ContextPath, IlpIntervalSolution


class ModelCheckFail( Exception ) :
    pass


class OptDirected( object ) :
    """
    Perform the optimization of the tag order with an ILP
    """

    def __init__( self, problem: IlpIntervalProblem, time_limit: int = 180 ) :
        # Base attributes
        self.problem: IlpIntervalProblem = problem
        self.M: int = 10_000
        self.model = LpProblem( name="Load_Tag_Optimization" )
        self.solver = CPLEX_CMD( timeLimit=time_limit, warmStart=True, msg=False )
        self.special_tag = "1"
        self.end_tag = "end"

        # Extracted from the problem.pb attributes
        self.all_tags: Set[ Tag ] = self.problem.extract_tag_set().union( [ self.special_tag, self.end_tag ] )

        # permutation has no type induction on r as combination has, thus giving a false py type check
        # noinspection PyTypeChecker
        self.directed_edges: List[ Tuple[ Tag, Tag ] ] = list( permutations( self.all_tags, r=2 ) )
        self.var_count: Dict[ str, int ] = { }
        self.constraint_count: Dict[ str, int ] = { }

        # Variables of the model

        # Variable used for the path modelisation
        self.dedge_vars: Dict[ (Tag, Tag), LpVariable ] = { }

        # Variable used for tag index modelisation
        self.vertex_entry_vars: Dict[ (Tag, Tag), LpVariable ] = { }
        self.vertex_exit_vars: Dict[ (Tag, Tag), LpVariable ] = { }
        self.vertex_index_vars: Dict[ Tag, LpVariable ] = { }

        # Variable used for the load index modelisation
        self.gt_index_vertex: Dict[ (Tag, Tag), LpVariable ] = { }
        self.load_index_vars: Dict[ LoadId, Dict[ Tag, LpVariable ] ] = { }
        self.virtual_load_index_vars: Dict[ LoadId, Dict[ Tag, int ] ] = { }
        self.phi_plus_vars: Dict[ LoadId, Dict[ Tag, LpVariable ] ] = { }
        self.lambda_plus_vars: Dict[ LoadId, Dict[ Tag, LpVariable ] ] = { }
        self.delta_plus_vars: Dict[ LoadId, Dict[ (Tag, Tag), LpVariable ] ] = { }
        self.delta_vars: Dict[ LoadId, Dict[ (Tag, Tag), LpVariable ] ] = { }
        self.load_index_absdiff_vars: Dict[ LoadId, Dict[ Tuple[ Tag, Tag ], LpVariable ] ] = { }

        # Variable used for the cost modelisation
        self.cost_partial_max: Dict[ LoadId, Dict[ FrozenSet[ Tag ], LpVariable ] ] = { }
        self.cost_unknown_max: Dict[ LoadId, LpVariable ] = { }

        # Construct the ILP model
        self._init_model()

        self.warm_up_init()

    def update_count( self, name: str ) :
        self.var_count[ name ] = self.model.numVariables()
        self.constraint_count[ name ] = self.model.numConstraints()

    def solve( self, lp_file: Path, solution_path: Path ) :
        print_color( "Start Solving ILP", Color.ORANGE )
        self.model.writeLP( str(lp_file.absolute()) )

        if "CPLEX_CMD" in pulp.listSolvers(True) and config.DEFAULT_AIT_PATH is None:
            self.model.fromJson( str(solution_path.absolute()) )
            self.model.solve( self.solver )
            with open( str(solution_path.absolute()).replace(".json", ".bin"), "wb" ) as f:
                pickle.dump( self.model, f )

        # if "CPLEX_CMD" in pulp.listSolvers(True) and config.DEFAULT_AIT_PATH is not None:
        #     with open( solution_path.absolute(), "wb" ) as f:
        #         pickle.dump( self.model, f )
        # else:
        #     with open( solution_path.absolute(), "rb" ) as f :
        #         self.model = pickle.load( f )
        print_color( "End Solving ILP", Color.ORANGE )

    def fake_edges( self, fake: List[ Tuple[ str, str ] ] ) :
        for fedge in fake :
            self.dedge_vars[ fedge ].setInitialValue( 1 )
            self.dedge_vars[ fedge ].fixValue()

    def print_result( self ) :
        for e, var in self.dedge_vars.items() :
            if round( var.varValue ) == 1 :
                print( f"{e}: {var.varValue}" )

        vertex_var_list = [ (t, var.varValue) for t, var in self.vertex_index_vars.items() ]
        vertex_var_list.sort( key=lambda t : t[ 1 ] )

        for t in vertex_var_list :
            print( f"{t[ 0 ]}: {t[ 1 ]}" )

        for i in range( len( vertex_var_list ) - 1 ) :
            t = (vertex_var_list[ i ][ 0 ], vertex_var_list[ i + 1 ][ 0 ])
            print( f"{t}: {self.dedge_vars[ t ].varValue}" )

    def check_result( self ) -> bool :
        for var in self.vertex_index_vars.values() :
            if var.varValue is None :
                return False

        for tag_dict in self.load_index_vars.values() :
            for var in tag_dict.values() :
                if var.varValue is None :
                    return False

        return True

    def compute_pre_interval( self, loadid: LoadId ) -> EqClassMap[ Tag ] :
        eqclass = EqClassMap( self.problem.path_contexts[ loadid ].tags )

        for t1, t2 in combinations( self.problem.path_contexts[ loadid ].tags, r=2 ) :
            if int( t1 ) - int( t2 ) in [ 1, -1 ] :
                eqclass.fusion_element_class( t1, t2 )

        return eqclass

    def compute_interval_cost( self, loadid: LoadId, interval: List[ Tag ] ) -> int :
        cost = 0
        for ec in self.problem.path_contexts[ loadid ].exacts :
            if ec.tag in interval :
                cost += ec.count

        for pc in self.problem.path_contexts[ loadid ].partial :
            for tag in pc.tags :
                if tag in interval :
                    cost += pc.count
                    break

        return cost

    def warmup_interval_order( self, loadid: LoadId ) :
        interval_eqclass = self.compute_pre_interval( loadid )

        interval_sorted = sorted(
            interval_eqclass.current_cls | pipe.map(
                lambda inter : (self.compute_interval_cost( loadid, list( interval_eqclass[ inter ] ) ), inter) ),
            key=lambda x : x[ 0 ]
        )

        for i, (_, interval_tag) in enumerate( interval_sorted ) :
            for tag in interval_eqclass[ interval_tag ] :
                self.load_index_vars[ loadid ][ tag ].setInitialValue( i + 1 )

    def warm_up_init( self ) :
        tag_sorted = sorted( self.all_tags | pipe.where( lambda x : x != self.special_tag and x != self.end_tag ),
                             key=lambda t : int( t ) )
        previous_tag = self.special_tag

        # Warm up the tags
        for tag in tag_sorted :
            self.dedge_vars[ (previous_tag, tag) ].setInitialValue( 1 )
            previous_tag = tag

        self.dedge_vars[ (previous_tag, self.end_tag) ].setInitialValue( 1 )

        for tag, tag2 in self.directed_edges :
            if self.dedge_vars[ (tag, tag2) ].varValue is None :
                self.dedge_vars[ (tag, tag2) ].setInitialValue( 0 )

        # Warm up the load intervals
        for context in self.problem.path_contexts :
            self.warmup_interval_order( context.loadid )

    def warm_up_from_previous_result( self, previous_res: IlpIntervalSolution ) :
        # The tags have already been warm up by the warmup_init, so we just focuses on the interval_orders

        for loadid, order in previous_res.interval_order.items() :
            if loadid in self.load_index_vars :
                for tag, value in order.items() :
                    new_tag = str( previous_res.tag_representation[ tag ] )
                    if new_tag in self.load_index_vars[ loadid ] :
                        self.load_index_vars[ loadid ][ new_tag ].setInitialValue( value )

        # The other loadids have been warmed up by the warmup_init.

    def _init_model( self ) :
        """Perform all the initialization of the model"""
        self._gen_path_def()

        self.update_count( "path_def" )
        self._gen_order_def()

        self.update_count( "index_interval" )
        self._gen_cost()

        self._gen_cost_constraints()
        self.update_count( "cost" )

    def _gen_path_def( self ) :
        """Path definition"""
        # * Edge definition
        self._gen_edge_def()
        self._gen_vertex_def()
        self._gen_vertex_index_def()

    def _gen_edge_def( self ) :
        # Generate the directed edges variable and the variables that fix the end of path
        self.dedge_vars = LpVariable.dicts( "de", self.directed_edges, cat=LpBinary )
        self.model += (lpSum( self.dedge_vars ) == len( self.all_tags ) - 1, f"dedge_sum")

    def _gen_vertex_def( self ) :
        # * Vertex definition
        self.vertex_entry_vars = LpVariable.dicts( "v_entry", self.all_tags, cat=LpBinary )
        for tag, var in self.vertex_entry_vars.items() :
            edge_var_tag = [ self.dedge_vars[ (t1, tag) ] for t1 in self.all_tags if t1 != tag ]
            self.model += (var == lpSum( edge_var_tag ), f"link_edges_vertex_entry_{tag}")
            val = 0 if tag == self.special_tag else 1
            self.model += (var == val, f"vertex_{tag}_entry_edge")

        self.vertex_exit_vars = LpVariable.dicts( "v_exit", self.all_tags, cat=LpBinary )
        for tag, var in self.vertex_exit_vars.items() :
            edge_var_tag = [ self.dedge_vars[ (tag, t2) ] for t2 in self.all_tags if t2 != tag ]
            self.model += (var == lpSum( edge_var_tag ), f"link_edges_vertex_exit_{tag}")
            val = 0 if tag == self.end_tag else 1
            self.model += (var == val, f"vertex_{tag}_exit_edge")

    def _gen_vertex_index_def( self ) :
        # * Vertex index definition
        n = len( self.all_tags )
        self.vertex_index_vars = LpVariable.dicts( "vi", self.all_tags, cat=LpInteger, upBound=n - 1, lowBound=0 )

        self.model += (self.vertex_index_vars[ self.special_tag ] == 0, f"special_vertex_index")
        self.model += (self.vertex_index_vars[ self.end_tag ] == n - 1, f"end_vertex_index")

        for tag, var in filter( lambda t : t[ 0 ] != self.special_tag and t[ 0 ] != self.end_tag,
                                self.vertex_index_vars.items() ) :
            for t1 in filter( lambda t : t != tag, self.all_tags ) :
                self.model += (var <= (self.vertex_index_vars[ t1 ] + 1) + n * (1 - self.dedge_vars[ (t1, tag) ]),
                               f"vertex_index_{t1}_{tag}_upBound")
                self.model += (var >= (self.vertex_index_vars[ t1 ] + 1) - n * (1 - self.dedge_vars[ (t1, tag) ]),
                               f"vertex_index_{t1}_{tag}_lowBound")

    def _gen_order_def( self ) :
        self._gen_index_interval()
        self._gen_phi_plus_def()
        self._gen_lambda_plus_def()
        self._gen_delta_plus_def()
        self._gen_delta_def()
        self._gen_vertex_index_gt()
        self._gen_delta_cs()

    def _gen_vertex_index_gt( self ) :
        for t1, t2 in permutations( self.all_tags, r=2 ) :
            self.gt_index_vertex[ (t1, t2) ] = ilp_gt( self.model, self.vertex_index_vars[ t1 ],
                                                       self.vertex_index_vars[ t2 ],
                                                       f"{t1}_{t2}", self.M )

    def _gen_index_interval( self ) :
        for load in self.problem.load_traversal() :
            loadid = load.loadid
            n = len( load.tags )

            if loadid in self.load_index_vars :
                continue

            self.load_index_vars[ loadid ] = LpVariable.dicts( f"O_{loadid}_", load.tags, lowBound=1, upBound=n,
                                                               cat=LpInteger )
            self.virtual_load_index_vars[ loadid ] = { }

            for i, tag in enumerate( filter( lambda t : t not in load.tags, self.all_tags ) ) :
                self.virtual_load_index_vars[ loadid ][ tag ] = n + i + 1

    def _gen_phi_plus_def( self ) :
        for load in self.problem.load_traversal() :
            n = len( self.all_tags )
            loadid = load.loadid

            if loadid in self.phi_plus_vars :
                continue

            self.phi_plus_vars[ loadid ] = LpVariable.dicts( f"phi_plus_{loadid}_", load.tags, cat=LpInteger,
                                                             lowBound=1, upBound=n )

            for tag in load.tags :
                prod_vars = [
                                prod_var_bin( self.model, self.load_index_vars[ loadid ][ t2 ],
                                              self.dedge_vars[ (tag, t2) ], f"phi_plus_prod_{loadid}_{tag}_{t2}" )
                                for t2 in load.tags if t2 != tag ] + [
                                self.virtual_load_index_vars[ loadid ][ t2 ] * self.dedge_vars[ (tag, t2) ]
                                for t2 in filter( lambda x : x not in load.tags, self.all_tags )
                            ]

                self.model += (
                    self.phi_plus_vars[ loadid ][ tag ] == lpSum( prod_vars ), f"phi_plus_{loadid}_{tag}_def"
                )

    def _gen_lambda_plus_def( self ) :
        for load in self.problem.load_traversal() :
            loadid = load.loadid

            if loadid in self.lambda_plus_vars :
                continue

            self.lambda_plus_vars[ loadid ] = LpVariable.dicts( f"lambda_plus_{loadid}_", self.all_tags, cat=LpBinary )

            for tag in self.all_tags :
                tag_edges = [ self.dedge_vars[ tag, t2 ] for t2 in load.tags if t2 != tag ]
                self.model += (
                    self.lambda_plus_vars[ loadid ][ tag ] == lpSum( tag_edges ), f"lambda_plus_{loadid}_{tag}_def")

    def _gen_delta_plus_def( self ) :
        for load in self.problem.load_traversal() :
            loadid = load.loadid
            n = len( self.all_tags )

            if loadid in self.delta_plus_vars :
                continue

            self.delta_plus_vars[ loadid ] = { }
            for t1, t2 in permutations( load.tags, r=2 ) :
                self.delta_plus_vars[ loadid ][ (t1, t2) ] = LpVariable( f"delta_plus_{loadid}_{t1}_{t2}",
                                                                         cat=LpInteger,
                                                                         lowBound=0, upBound=n )

                phi_plus = self.phi_plus_vars[ loadid ][ t1 ]
                phi = self.load_index_vars[ loadid ][ t2 ]

                _, _, _, abs_expr = ilp_absdiff( self.model, phi_plus, phi, f"delta_plus_{loadid}_{t1}_{t2}_abs",
                                                 self.M )

                self.model += (
                    self.delta_plus_vars[ loadid ][ (t1, t2) ] == abs_expr, f"delta_plus_{loadid}_{t1}_{t2}_def")

    def _gen_delta_def( self ) :
        for load in self.problem.load_traversal() :
            loadid = load.loadid
            n = len( load.tags )

            if loadid in self.delta_vars :
                continue

            self.delta_vars[ loadid ] = { }
            for t1, t2 in combinations( load.tags, r=2 ) :
                self.delta_vars[ loadid ][ (t1, t2) ] = LpVariable( f"delta_{loadid}_{t1}_{t2}", cat=LpInteger,
                                                                    lowBound=0, upBound=n )
                self.delta_vars[ loadid ][ (t2, t1) ] = self.delta_vars[ loadid ][ (t1, t2) ]

                phi1 = self.load_index_vars[ loadid ][ t1 ]
                phi2 = self.load_index_vars[ loadid ][ t2 ]

                _, _, _, abs_expr = ilp_absdiff( self.model, phi1, phi2, f"delta_{loadid}_{t1}_{t2}_abs",
                                                 self.M )

                self.model += (self.delta_vars[ loadid ][ (t1, t2) ] == abs_expr, f"delta_{loadid}_{t1}_{t2}_def")

    def _gen_delta_cs( self ) :
        loadid_set = set()
        for load in self.problem.load_traversal() :
            loadid = load.loadid
            n = len( self.all_tags )

            if loadid in loadid_set :
                continue

            loadid_set.update( [loadid] )

            for t1, t2 in combinations( load.tags, r=2 ) :
                r1_gt_r2 = self.gt_index_vertex[ (t1, t2) ]
                r2_gt_r1 = self.gt_index_vertex[ (t2, t1) ]

                dplus_t1_t2 = self.delta_plus_vars[ loadid ][ (t1, t2) ]
                dplus_t2_t1 = self.delta_plus_vars[ loadid ][ (t2, t1) ]

                gt_prod_dplus_t1_t2 = prod_var_bin( self.model, dplus_t2_t1, r1_gt_r2, f"gt_dplus_{loadid}_{t1}_{t2}" )
                gt_prod_dplus_t2_t1 = prod_var_bin( self.model, dplus_t1_t2, r2_gt_r1, f"gt_dplus_{loadid}_{t2}_{t1}" )

                pseudo_abs = LpVariable( f"dplus_abs_{loadid}_{t1}_{t2}", cat=LpInteger, lowBound=0,
                                         upBound=n )
                self.model += (
                    pseudo_abs == gt_prod_dplus_t2_t1 + gt_prod_dplus_t1_t2, f"dplus_abs_{loadid}_{t1}_{t2}_def")

                lplus_t1 = self.lambda_plus_vars[ loadid ][ t1 ]
                lplus_t2 = self.lambda_plus_vars[ loadid ][ t2 ]

                Lambda_t1_t2 = bin_or( self.model, [ lplus_t1, lplus_t2 ], f"Lambda_{loadid}_{t1}_{t2}" )

                prod_Lambda_pabs = prod_var_bin( self.model, pseudo_abs, Lambda_t1_t2,
                                                 f"LambdaPlus_dplus_abs_{loadid}_{t1}_{t2}" )

                delta = self.delta_vars[ loadid ][ (t1, t2) ]
                self.model += (
                    delta <= prod_Lambda_pabs + self.M - Lambda_t1_t2 * self.M, f"upB_delta_{loadid}_{t1}_{t2}")
                self.model += (delta >= prod_Lambda_pabs + 1 - Lambda_t1_t2, f"lowB_delta_{loadid}_{t1}_{t2}")

    def _gen_cost_expr( self, path: ContextPath, name: str ) -> LpAffineExpression :
        expr: LpAffineExpression = LpAffineExpression()

        for load in path :
            loadid = load.loadid

            if loadid not in self.cost_partial_max :
                self.cost_partial_max[ loadid ] = { }

            # Exact contexts
            for exact_context in load.exacts :
                O_t = self.load_index_vars[ loadid ][ exact_context.tag ]
                expr += exact_context.count * O_t

            # Partial contexts
            for i, partial_context in enumerate( load.partial ) :
                ts = frozenset( partial_context.tags )
                if ts not in self.cost_partial_max[ loadid ] :
                    O_ts = [ (t, self.load_index_vars[ loadid ][ t ]) for t in partial_context.tags ]
                    max_O_ts = LpVariable( f"max_partial_{loadid}_{i}_{name}", cat=LpInteger, lowBound=1,
                                           upBound=len( load.tags ) )

                    for t, O_t in O_ts :
                        self.model += (max_O_ts >= O_t, f"lowBound_{loadid}_partial_{i}_O{t}_{name}")

                    self.cost_partial_max[ loadid ][ ts ] = max_O_ts

                max_O_ts = self.cost_partial_max[ loadid ][ ts ]
                expr += partial_context.count * max_O_ts

            # Unknown contexts
            for unknown_contex in load.unknown :
                if loadid not in self.cost_unknown_max :
                    self.cost_unknown_max[ loadid ] = LpVariable( f"max_unkown_{loadid}", cat=LpInteger, lowBound=1,
                                                                  upBound=len( load.tags ) )
                    max_O_ts = self.cost_unknown_max[ loadid ]

                    for t, O_t in self.load_index_vars[ loadid ].items() :
                        self.model += (max_O_ts >= O_t, f"lowBound_{loadid}_unknown_O{t}_{name}")

                max_O_ts = self.cost_unknown_max[ loadid ]

                expr += unknown_contex.count * max_O_ts

        return expr

    def _gen_cost( self ) :
        # GOAL
        self.model += self._gen_cost_expr( self.problem.path_contexts, name="current" )

    def _gen_cost_constraints( self ) :
        for i, (path, cost) in enumerate( self.problem.constraints ) :
            self.model += (self._gen_cost_expr( path, name=f"previous_{i}" ) <= cost, f"constraint_{i}")
