# RT-DFI

RT-DFI tool for WCET improved Data-Flow Integrity

This repository contains the tools used to obtain the results presented in "RT-DFI: Optimizing Data-Flow Integrity for
Real-Time Systems, Bellec et al., ECRTS'22"

The current state of the code is similar to the one used for the experiments however, it requires CPLEX and aiT RISCV installed on the machine.
