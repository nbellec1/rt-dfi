# coding=utf-8
from optDfi.optimizations.ilp_opt.ilp_problem.context import ExactContext, PartialContext, UnknownContext, LoadContexts
from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import EqClassMap
from optDfi.optimizations.ilp_opt.ilp_problem.problem import IlpIntervalProblem, ContextPath
from optDfi.optimizations.ilp_opt.ilp_opt import OptDirected
from optDfi.optimizations.ilp_opt.tag_check_ilp_opt import reconstruct_tag_representation, reconstruct_interval_order

EC = ExactContext
PC = PartialContext
UC = UnknownContext
CP = ContextPath
LC = LoadContexts
IIP = IlpIntervalProblem


def test_reconstruct_tag_representation() :
    tags = { '4', '5', '8', '1', '3', '2' }
    tag_eqclass = EqClassMap( tags )

    tag_eqclass.fusion_element_class( '4', '5' )
    tag_eqclass.fusion_element_class( '4', '8' )
    tag_eqclass.fusion_element_class( '3', '2' )

    witnessA = tag_eqclass.witness( '4' )
    witnessE = tag_eqclass.witness( '3' )
    witnessD = tag_eqclass.witness( '1' )

    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( witnessA, 100 ),
            ],
            partial=[
                PC( frozenset( (witnessA, witnessD) ), 80 ),
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ witnessA, witnessD } ),
        LC( loadid='lid2',
            exacts=[
                EC( witnessD, 100 ),
                EC( witnessE, 25 )
            ],
            partial=[
                PC( frozenset( (witnessE, witnessD) ), 80 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ witnessD, witnessE } ),
    ] )

    iip1 = IIP(
        path_contexts=cp1,
        constraints=[ ],
        eqclass_tags=tag_eqclass,
        eqclass_loads=EqClassMap()
    )

    problem = OptDirected( iip1 )

    problem.vertex_index_vars[ witnessA ].varValue = 0
    problem.vertex_index_vars[ witnessD ].varValue = 1
    problem.vertex_index_vars[ witnessE ].varValue = 2

    representation = reconstruct_tag_representation( problem, iip1 )

    assert { representation[ t ] for t in tag_eqclass[ witnessA ] } == { 2, 3, 4 }
    assert { representation[ t ] for t in tag_eqclass[ witnessE ] } == { 6, 7 }
    assert { representation[ t ] for t in tag_eqclass[ witnessD ] } == { 5 }


def test_reconstruct_interval_order() :
    loadid = { 'lid1', 'lid2', 'lid3', 'lid4', 'lid5', 'lid6' }
    load_eqclass = EqClassMap( loadid )

    load_eqclass.fusion_element_class( 'lid1', 'lid2' )
    load_eqclass.fusion_element_class( 'lid1', 'lid3' )
    load_eqclass.fusion_element_class( 'lid5', 'lid6' )

    witnessLid1 = load_eqclass.witness( 'lid1' )
    witnessLid4 = load_eqclass.witness( 'lid4' )
    witnessLid5 = load_eqclass.witness( 'lid5' )

    witnessA = '2'
    witnessD = '3'
    witnessE = '4'

    cp1 = CP( [
        LC( loadid=witnessLid1,
            exacts=[
                EC( witnessA, 100 ),
            ],
            partial=[
                PC( frozenset( (witnessA, witnessD) ), 80 ),
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ witnessA, witnessD, witnessE } ),
        LC( loadid=witnessLid4,
            exacts=[
                EC( witnessD, 100 ),
                EC( witnessE, 25 )
            ],
            partial=[
                PC( frozenset( (witnessE, witnessD) ), 80 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ witnessD, witnessE } ),
        LC( loadid=witnessLid5,
            exacts=[
                EC( witnessA, 100 ),
            ],
            partial=[
                PC( frozenset( (witnessE, witnessA) ), 80 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ witnessA, witnessE } ),
    ] )

    iip1 = IIP(
        path_contexts=cp1,
        constraints=[ ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=load_eqclass
    )

    problem = OptDirected( iip1 )

    problem.load_index_vars[ witnessLid1 ][ witnessA ].varValue = 1
    problem.load_index_vars[ witnessLid1 ][ witnessD ].varValue = 2
    problem.load_index_vars[ witnessLid1 ][ witnessE ].varValue = 2

    problem.load_index_vars[ witnessLid4 ][ witnessD ].varValue = 1
    problem.load_index_vars[ witnessLid4 ][ witnessE ].varValue = 1

    problem.load_index_vars[ witnessLid5 ][ witnessA ].varValue = 2
    problem.load_index_vars[ witnessLid5 ][ witnessE ].varValue = 1

    representation = reconstruct_interval_order( problem, iip1 )

    for loadid in [ 'lid1', 'lid2', 'lid3' ] :
        assert representation[ loadid ][ witnessA ] == 1
        assert representation[ loadid ][ witnessD ] == 2
        assert representation[ loadid ][ witnessE ] == 2

    assert representation[ 'lid4' ][ witnessD ] == 1
    assert representation[ 'lid4' ][ witnessE ] == 1

    for loadid in [ 'lid5', 'lid6' ] :
        assert representation[ loadid ][ witnessE ] == 1
        assert representation[ loadid ][ witnessA ] == 2
